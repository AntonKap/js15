let btn = document.querySelector(".btn");
let text = document.querySelector(".btnTxt");
let minutesElem = document.querySelector(".minutes");
let secondsElem = document.querySelector(".seconds");
let startButton = document.querySelector(".start");
let stopButton = document.querySelector(".stop");
let resetButton = document.querySelector(".reset");
let doublePoint= document.querySelector(".dat");
let i = null;
let timeout = null;
let IdInterval = null;
let min = 9;
let sec = 59;

//1
btn.addEventListener("click", (e) => {
  clearTimeout(timeout);
  timeout = setTimeout(() => {
    i += 1;
    text.textContent = `the operation was completed successfully ${i}`;
  }, 1000);
});

//2
function propTime(time) {
  return time.toString().length > 1 ? time : `0${time}`;
}

startButton.addEventListener("click", () => {
    
  clearInterval(IdInterval);
  doublePoint.textContent = ":"
  doublePoint.classList.remove("psize");
  IdInterval = setInterval(() => {
    sec -= 1;
    if (sec <= 0) {
      min -= 1;
      sec = 59;
    } else if (min <= 0) {
      clearInterval(IdInterval);
      min = 10;
      sec = 0;
      minutesElem.textContent = "";
      secondsElem.textContent = "";
      doublePoint.textContent = "countdown is complete"
      doublePoint.classList.add("psize")
      return;
    }
    secondsElem.textContent = propTime(sec);
    minutesElem.textContent = propTime(min);
  }, 1000);
});

stopButton.addEventListener("click", () => {
  clearInterval(IdInterval);
});

resetButton.addEventListener("click", () => {
  clearInterval(IdInterval);
  min = 10;
  sec = 0;
  minutesElem.textContent = "10";
  secondsElem.textContent = "00";
  doublePoint.textContent = ":"
  doublePoint.classList.remove("psize");
});
